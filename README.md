# README #

### What is this repository for? ###

This application has been created for private use, as a tool to support language learning process. It allows to convert the offline version of Lexin swedish-swedish dictionary to the Kindle dictionary compatible format.

The application was designed to be as extensible as possible in terms of the possibility of replacing the default dictionary source XML parser with the new ones, which support other dictionaries. The class providing a new parser only needs to extend AbstractParser class.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND

### How do I get set up? ###

Standalone jar with all the required dependencies can be built using the following Maven command:
```
mvn clean compile assembly:single install
```

External libraries used in the project:

1. Logging: [Log4j](https://logging.apache.org/log4j/2.x/)
2. Parsing command line arguments: [Args4j](http://args4j.kohsuke.org/)
3. Testing: [JUnit](https://junit.org/junit4/), [Mockito](http://site.mockito.org/)

#### Running the application ####

The project contains a [sample file](https://bitbucket.org/vessla/lexin-to-kindle-dictionary-converter/src/master/LEXIN_sample.xml) with several dictionary entries to test, whether the application is running correctly. However, in order to create a complete dictionary, Lexin source XML file needs to be downloaded from [here](https://spraakbanken.gu.se/eng/resource-info-lexin).

Once the source file is downloaded, the application can be used to convert it to the Kindle-compatible format.

WARNING: By default application writes the output file to the ["kindle" directory](https://bitbucket.org/vessla/lexin-to-kindle-dictionary-converter/src/master/kindle/).

Application parameters:
```

-i [input file]			lexin dictionary file name (default value "LEXIN_sample.xml")
-o [output file]	    output file name (default value "kindle/pagea.html")

```

Converting Lexin XML to Kindle Dictionary (example):
```
java -jar lexintokindle-standalone.jar -i LEXIN.xml -o kindle/pagea.html
```

#### Exporting the output to .mobi file ####

1. Download the [KindleGen](https://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211) tool
2. Use the files in the [kindle directory](https://bitbucket.org/vessla/lexin-to-kindle-dictionary-converter/src/master/kindle/) along with the generated pagea.html file to generate the .mobi file using the command presented below:
```
kindlegen swedishdict.opf
```

### Who do I talk to? ###

pjadamska@gmail.com