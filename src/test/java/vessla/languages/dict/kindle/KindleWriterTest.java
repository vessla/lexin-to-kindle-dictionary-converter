package vessla.languages.dict.kindle;

import org.junit.Test;

import org.mockito.Mockito;
import vessla.languages.dict.kindle.parser.AbstractParser;
import vessla.languages.dict.kindle.parser.LexinParser;
import vessla.languages.dict.kindle.writer.KindleWriter;

import java.io.StringWriter;

import static org.junit.Assert.assertEquals;

public class KindleWriterTest {

    @Test
    public void shouldWriteRedirectEntryWithHeaderAndFooter()
    {
        StringWriter testedWriter = new StringWriter();
        AbstractParser testedParser = new LexinParser("XML/KindleWriterTest/test_shouldWriteRedirectEntryWithHeaderAndFooter.xml", new KindleWriter(testedWriter));
        testedParser.parse();

        StringBuilder expectedWrittenEntry = new StringBuilder("<!DOCTYPE html>\n");
        expectedWrittenEntry.append("<html xmlns:mbp=\"https://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf\"\n")
                .append("xmlns:idx=\"https://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf\">\n")
                .append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>\n")
                .append("<mbp:frameset>\n")
                .append("<idx:entry name=\"headword\" scriptable=\"yes\" spell=\"yes\">\n")
                .append("\t<idx:orth value=\"bröt\"><b>bröt</b>\n\t</idx:orth>\n")
                .append("\t<p><em>(se bryter)</em></p>\n</idx:entry>")
                .append("\n</mbp:frameset>\n</body>\n</html>");
        assertEquals(expectedWrittenEntry.toString(), testedWriter.toString());
    }

    @Test
    public void shouldWriteVerbWithNoDefinitionMeaning()
    {
        StringWriter testedWriter = new StringWriter();
        AbstractParser testedParser = new LexinParser("XML/KindleWriterTest/test_shouldWriteVerbWithNoDefinitionMeaning.xml", new KindleWriter(testedWriter));
        testedParser.parse();

        StringBuilder expectedWrittenEntry = new StringBuilder("<!DOCTYPE html>\n");
        expectedWrittenEntry.append("<html xmlns:mbp=\"https://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf\"\n")
                .append("xmlns:idx=\"https://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf\">\n")
                .append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>\n")
                .append("<mbp:frameset>\n")
                .append("<idx:entry name=\"headword\" scriptable=\"yes\" spell=\"yes\">\n")
                .append("\t<idx:orth value=\"glufsar\"><b>glufsar</b>\n")
                .append("\t\t<idx:infl>\n\t\t\t<idx:iform value=\"glufsade\"></idx:iform>\n")
                .append("\t\t\t<idx:iform value=\"glufsat\"></idx:iform>\n\t\t\t<idx:iform value=\"glufsa\"></idx:iform>\n")
	            .append("\t\t</idx:infl>\n\t</idx:orth>\n\t<p><em>(verb)</em>glufsade glufsat glufsa(!)</p>\n")
                .append("\t<p>1</p>\n\t\t<p style=\"padding:4em\">Uttryck</p>\n")
                .append("\t\t<p style=\"padding:6em\">glufsa i sig (\"äta glupskt\")</p>\n</idx:entry>\n")
                .append("</mbp:frameset>\n</body>\n</html>");
        assertEquals(expectedWrittenEntry.toString(), testedWriter.toString());
    }

    @Test
    public void shouldWriteSubstantiveWithInflectionBasedIndexes()
    {
        StringWriter testedWriter = new StringWriter();
        KindleWriter kindleWriter = Mockito.spy(new KindleWriter(testedWriter));
        AbstractParser testedParser = new LexinParser("XML/KindleWriterTest/test_shouldWriteSubstantiveWithInflectionBasedIndexes.xml", kindleWriter);

        //HTML file header and footer will not be written to only verify the entry itself
        Mockito.doNothing().when(kindleWriter).onParsingStarted();
        Mockito.doNothing().when(kindleWriter).onParsingComplete();

        testedParser.parse();

        StringBuilder expectedWrittenEntry = new StringBuilder("<idx:entry name=\"headword\" scriptable=\"yes\" spell=\"yes\">\n");
        expectedWrittenEntry.append("\t<idx:orth value=\"bråkdel\"><b>bråk~del</b>\n")
                .append("\t\t<idx:infl>\n\t\t\t<idx:iform value=\"bråkdelen\"></idx:iform>\n")
                .append("\t\t\t<idx:iform value=\"bråkdelar\"></idx:iform>\n")
                .append("\t\t</idx:infl>\n\t</idx:orth>\n\t<p><em>(subst.)</em>-delen -delar</p>\n")
                .append("</idx:entry>\n");
        assertEquals(expectedWrittenEntry.toString(), testedWriter.toString());
    }

    @Test
    public void shouldWriteSubstantiveWithCompoundBasedIndexes()
    {
        StringWriter testedWriter = new StringWriter();
        KindleWriter kindleWriter = Mockito.spy(new KindleWriter(testedWriter));
        AbstractParser testedParser = new LexinParser("XML/KindleWriterTest/test_shouldWriteSubstantiveWithCompoundBasedIndexes.xml", kindleWriter);

        //HTML file header and footer will not be written to only verify the entry itself
        Mockito.doNothing().when(kindleWriter).onParsingStarted();
        Mockito.doNothing().when(kindleWriter).onParsingComplete();

        testedParser.parse();

        StringBuilder expectedWrittenEntry = new StringBuilder("<idx:entry name=\"headword\" scriptable=\"yes\" spell=\"yes\">\n");
        expectedWrittenEntry.append("\t<idx:orth value=\"citron\"><b>citron</b>\n")
                .append("\t\t<idx:infl>\n\t\t\t<idx:iform value=\"citronklyfta\"></idx:iform>\n")
                .append("\t\t\t<idx:iform value=\"citronklyftan\"></idx:iform>\n")
                .append("\t\t\t<idx:iform value=\"citronvadå\"></idx:iform>\n")
                .append("\t\t\t<idx:iform value=\"citronvadået\"></idx:iform>\n")
                .append("\t\t\t<idx:iform value=\"citronvadsomhelst\"></idx:iform>\n")
                .append("\t\t</idx:infl>\n\t</idx:orth>\n\t<p></p>\n")
                .append("\t<p>1 en frukt</p>\n\t\t<p style=\"padding:4em\">Uttryck</p>\n")
                .append("\t\t<p style=\"padding:6em\">tala med bruten röst</p>\n")
                .append("\t\t<p style=\"padding:6em\">citron~klyfta -n</p>\n")
                .append("\t<p>2</p>\n\t<p>  <em>om malm \"utvinna\"</em></p>\n")
	            .append("\t\t<p style=\"padding:4em\">Exempel</p>\n")
		        .append("\t\t<p style=\"padding:6em\">bryta mot lagen</p>\n")
                .append("\t\t<p style=\"padding:4em\">Uttryck</p>\n")
                .append("\t\t<p style=\"padding:6em\">citron~vadå -et</p>\n")
                .append("\t\t<p style=\"padding:6em\">citron~vadsomhelst</p>\n")
                .append("</idx:entry>\n");
        assertEquals(expectedWrittenEntry.toString(), testedWriter.toString());
    }
}
