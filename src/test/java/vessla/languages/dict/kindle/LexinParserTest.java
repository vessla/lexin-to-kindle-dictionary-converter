package vessla.languages.dict.kindle;

import org.junit.Test;
import org.mockito.Mockito;
import vessla.languages.dict.kindle.model.DictionaryEntry;
import vessla.languages.dict.kindle.model.DictionaryEntryMeaning;
import vessla.languages.dict.kindle.parser.AbstractParser;
import vessla.languages.dict.kindle.parser.LexinParser;
import vessla.languages.dict.kindle.parser.ParserListener;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Unit test for Lexin parser.
 */
public class LexinParserTest {

    @Test
    public void shouldReturnRedirectEntry() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnRedirectEntry.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("bröt", newEntry.getBaseForm());
                assertEquals("se bryter", newEntry.getType());
                assertTrue(newEntry.isRedirectEntry());
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });

        testedParser.parse();
    }

    @Test
    public void shouldReturnVerbWithTwoMeanings() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnVerbWithTwoMeanings.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("bryter upp", newEntry.getBaseForm());
                assertEquals("bröt brutit bryt! bryta", newEntry.getInflection());
                assertArrayEquals(new String[]{"bröt", "brutit", "bryt", "bryta"}, newEntry.getIndexes().toArray());
                assertEquals("verb", newEntry.getType());
                assertFalse(newEntry.isRedirectEntry());
                ArrayList<DictionaryEntryMeaning> meanings = newEntry.getMeanings();
                assertEquals(2, meanings.size());
                assertEquals("komma i konflikt med (lagar etc), inte uppfylla, kränka", meanings.get(0).getDefinition());
                assertEquals(2, meanings.get(0).getExamples().size());
                assertEquals("bryta mot lagen", meanings.get(0).getExamples().get(0));
                assertEquals("bryta ett löfte", meanings.get(0).getExamples().get(1));
                assertEquals("(få att) gå i bitar", meanings.get(1).getDefinition());
                assertEquals(1, meanings.get(1).getUsages().size());
                assertEquals("om malm \"utvinna\"; om vågor \"slås till skum\"; om ljus \"reflekteras\"", meanings.get(1).getUsages().get(0));
                assertEquals(3, meanings.get(1).getExamples().size());
                assertEquals("ramla och bryta benet", meanings.get(1).getExamples().get(0));
                assertEquals("bryta malm", meanings.get(1).getExamples().get(1));
                assertEquals("vågorna bröt(s) mot klipporna", meanings.get(1).getExamples().get(2));
                assertEquals(1, meanings.get(1).getIdioms().size());
                assertEquals("tala med bruten röst (\"tala med svag och darrande röst\")", meanings.get(1).getIdioms().get(0));
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });

        testedParser.parse();
    }

    @Test
    public void shouldReturnSubstantiveWithCompounds() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnSubstantiveWithCompounds.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("sallads~huvud", newEntry.getBaseForm());
                assertEquals("salladshuvud", newEntry.getBaseIndex());
                assertEquals("salladen sallader", newEntry.getInflection());
                assertArrayEquals(new String[]{"salladen", "sallader", "salladshuvud", "salladshuvudet", "västkustsallad"}, newEntry.getIndexes().toArray());
                assertEquals("subst.", newEntry.getType());
                assertFalse(newEntry.isRedirectEntry());
                ArrayList<DictionaryEntryMeaning> meanings = newEntry.getMeanings();
                assertEquals(2, meanings.size());
                assertEquals("en grönsak med stora, gröna blad", meanings.get(0).getDefinition());
                assertEquals(1, meanings.get(0).getCompounds().size());
                assertEquals("sallads~huvud -et", meanings.get(0).getCompounds().get(0));
                assertEquals("en maträtt huvudsakligen bestående av råa eller kokta grönsaker i bitar", meanings.get(1).getDefinition());
                assertEquals("västkustsallad", meanings.get(1).getCompounds().get(0));
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });

        testedParser.parse();
    }

    @Test
    public void shouldReturnVerbWithNoDefinitionMeaning() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnVerbWithNoDefinitionMeaning.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("glufsar", newEntry.getBaseForm());
                assertEquals("glufsade glufsat glufsa(!)", newEntry.getInflection());
                assertArrayEquals(new String[]{"glufsade", "glufsat", "glufsa"}, newEntry.getIndexes().toArray());
                assertEquals("verb", newEntry.getType());
                assertFalse(newEntry.isRedirectEntry());
                ArrayList<DictionaryEntryMeaning> meanings = newEntry.getMeanings();
                assertEquals(1, meanings.size());
                assertNull(meanings.get(0).getDefinition());
                assertEquals(1, meanings.get(0).getIdioms().size());
                assertEquals("glufsa i sig (\"äta glupskt\")", meanings.get(0).getIdioms().get(0));
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });

        testedParser.parse();
    }

    @Test
    public void shouldReturnAdjectiveWithBaseFormNumberRemoved() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnAdjectiveWithBaseFormNumberRemoved.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("rasande", newEntry.getBaseForm());
                assertEquals("adj.", newEntry.getType());
                assertFalse(newEntry.isRedirectEntry());
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });

        testedParser.parse();
    }

    @Test
    public void shouldReturnSubstantiveWithInflectionBasedIndexes() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnSubstantiveWithInflectionBasedIndexes.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("bråk~del", newEntry.getBaseForm());
                assertEquals("bråkdel", newEntry.getBaseIndex());
                assertEquals("-delen -delar", newEntry.getInflection());
                assertArrayEquals(new String[]{"bråkdelen", "bråkdelar"}, newEntry.getIndexes().toArray());
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });

        testedParser.parse();
    }

    @Test
    public void shouldReturnSubstantiveWithCompoundBasedIndexes() {
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnSubstantiveWithCompoundBasedIndexes.xml", new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
                assertEquals("citron", newEntry.getBaseForm());
                assertArrayEquals(new String[]{"citronklyfta", "citronklyftan", "citronvadå", "citronvadået", "citronvadsomhelst"}, newEntry.getIndexes().toArray());
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });
        testedParser.parse();
    }

    @Test
    public void shouldInvokeParserListenerCallbacksOnce() {
        ParserListener listener = Mockito.spy(new ParserListener() {
            @Override
            public void onParsingStarted() {
            }

            @Override
            public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
            }

            @Override
            public void onParsingError(Exception parsingException) {
            }

            @Override
            public void onParsingComplete() {
            }
        });
        AbstractParser testedParser = new LexinParser("XML/LexinParserTest/test_shouldReturnSubstantiveWithCompoundBasedIndexes.xml", listener);
        testedParser.parse();
        Mockito.verify(listener, Mockito.times(1)).onParsingStarted();
        Mockito.verify(listener, Mockito.times(1)).onParsingComplete();
    }
}
