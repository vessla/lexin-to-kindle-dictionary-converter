package vessla.languages.dict.kindle.model;

import java.util.ArrayList;

public class DictionaryEntry {

	private String baseForm;
    private String baseIndex;
	private String type;
	private String inflection;
	private boolean isRedirectEntry;
	private ArrayList<DictionaryEntryMeaning> meanings = new ArrayList<>();
	private ArrayList<String> indexes = new ArrayList<>();

	//------------------------------------------------------------------------------------------------------
	// Getters & setters
	//------------------------------------------------------------------------------------------------------

	public String getBaseForm() {
		return baseForm;
	}

    public String getBaseIndex() { return baseIndex; }

    public void setBaseIndex(String baseIndex) { this.baseIndex = baseIndex; }

    public void setBaseForm(String baseForm) {
		this.baseForm = baseForm;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInflection() {
		return inflection;
	}

	public void setInflection(String inflection) {
		this.inflection = inflection;
	}

	public boolean isRedirectEntry() { return isRedirectEntry; }

	public void setRedirectEntry(boolean redirectEntry) { isRedirectEntry = redirectEntry; }

	public ArrayList<DictionaryEntryMeaning> getMeanings() {
		return meanings;
	}

	public ArrayList<String> getIndexes() { return indexes; }

}
