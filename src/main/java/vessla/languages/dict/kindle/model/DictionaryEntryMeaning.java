package vessla.languages.dict.kindle.model;

import java.util.ArrayList;

public class DictionaryEntryMeaning {
	
	private String definition;
	private ArrayList<String> examples = new ArrayList<>();
	private ArrayList<String> idioms = new ArrayList<>();
	private ArrayList<String> usages = new ArrayList<>();
	private ArrayList<String> compounds = new ArrayList<>();

	//------------------------------------------------------------------------------------------------------
	// Getters & setters
	//------------------------------------------------------------------------------------------------------

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public ArrayList<String> getExamples() {
		return examples;
	}

	public void setExamples(ArrayList<String> examples) {
		this.examples = examples;
	}

	public ArrayList<String> getIdioms() {
		return idioms;
	}

	public void setIdioms(ArrayList<String> idioms) {
		this.idioms = idioms;
	}

	public ArrayList<String> getUsages() {
		return usages;
	}

	public void setUsages(ArrayList<String> usages) {
		this.usages = usages;
	}

	public ArrayList<String> getCompounds() {
		return compounds;
	}

	public void setCompounds(ArrayList<String> compounds) {
		this.compounds = compounds;
	}
}
