package vessla.languages.dict.kindle.parser;

import org.apache.log4j.Logger;
import vessla.languages.dict.kindle.model.DictionaryEntry;
import vessla.languages.dict.kindle.model.DictionaryEntryMeaning;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;

public class LexinParser extends AbstractParser {

    private static Logger logger = Logger.getLogger(LexinParser.class);

    public LexinParser(String sourceFile, ParserListener parserListener) {
        super(sourceFile, parserListener);
    }

    @Override
    protected String getStartEntryTag() {
        return "lemma-entry";
    }

    @Override
    protected DictionaryEntry parseSingleEntry(XMLEventReader xmlEventReader) throws XMLStreamException{
        DictionaryEntry parsedEntry = new DictionaryEntry();
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                String tag = startElement.getName().getLocalPart();
                switch (tag) {
                    case "form":
                        String baseForm = xmlEventReader.nextEvent().asCharacters().getData().replaceAll(" (\\d)+","");
                        parsedEntry.setBaseForm(baseForm);
                        parsedEntry.setBaseIndex(baseForm.replaceAll("~", ""));
                        break;
                    case "pos": //adj., verb, etc. or "see ...."
                        XMLEvent nextEvent = xmlEventReader.nextEvent();
                        if(!(nextEvent.isEndElement())) {
                            parsedEntry.setType(nextEvent.asCharacters().getData());
                            parsedEntry.setRedirectEntry(parsedEntry.getType().startsWith("se "));
                        }
                        break;
                    case "inflection":
                        nextEvent = xmlEventReader.nextEvent();
                        if(!(nextEvent.isEndElement())){
                            String inflection = nextEvent.asCharacters().getData();
                            parsedEntry.setInflection(inflection);
                            parsedEntry.getIndexes().addAll(extractIndexesFromInflection(parsedEntry));
                        }
                        break;
                    case "lexeme":
                        parsedEntry.getMeanings().add(new DictionaryEntryMeaning());
                        break;
                        case "definition":
                            parsedEntry.getMeanings().get(parsedEntry.getMeanings().size() - 1).setDefinition(xmlEventReader.nextEvent().asCharacters().getData());
                            break;
                        case "example":
                            parsedEntry.getMeanings().get(parsedEntry.getMeanings().size() - 1).getExamples().add(xmlEventReader.nextEvent().asCharacters().getData());
                            break;
                        case "usage":
                            parsedEntry.getMeanings().get(parsedEntry.getMeanings().size() - 1).getUsages().add(xmlEventReader.nextEvent().asCharacters().getData());
                            break;
                        case "idiom":
                            parsedEntry.getMeanings().get(parsedEntry.getMeanings().size() - 1).getIdioms().add(xmlEventReader.nextEvent().asCharacters().getData());
                            break;
                        case "compound":
                            String compound = xmlEventReader.nextEvent().asCharacters().getData();
                            parsedEntry.getMeanings().get(parsedEntry.getMeanings().size() - 1).getCompounds().add(compound);
                            parsedEntry.getIndexes().addAll(extractIndexesFromCompounds(compound));
                            break;
                    }

                } else if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("lemma-entry")) {
                        logger.debug(parsedEntry.getBaseForm());
                        break;
                    }
                }
            }

        return parsedEntry;
    }

    private static ArrayList<String> extractIndexesFromInflection(DictionaryEntry processedEntry){
        ArrayList<String> result = new ArrayList<>();
        String indexFromInflection;
        for(String inflection:processedEntry.getInflection().split(" ")){
            //Not merging "(", "!" ")" into "(!)" in replaceAll due to inconsistent notation in the source file
            indexFromInflection = inflection.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("!", "").replaceAll("~", "");
            String[] compoundFormParts = processedEntry.getBaseForm().split("~");
            if(!indexFromInflection.equals(""))
                result.add(compoundFormParts.length>1?indexFromInflection.replaceFirst("-", compoundFormParts[0]):indexFromInflection);
        }
        return result;
    }

    private static ArrayList<String> extractIndexesFromCompounds(String processedCompound){
        ArrayList<String> result = new ArrayList<>();
        String indexFromCompounds;
        indexFromCompounds = processedCompound.replaceAll("~", "");
        String[] compoundFormParts = indexFromCompounds.split(" -");
        result.add(compoundFormParts[0]);
        if(compoundFormParts.length>1)
            result.add(compoundFormParts[0]+compoundFormParts[1]);

        return result;
    }
}
