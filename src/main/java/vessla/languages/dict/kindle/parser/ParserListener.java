package vessla.languages.dict.kindle.parser;

import vessla.languages.dict.kindle.model.DictionaryEntry;

public interface ParserListener {

    void onParsingStarted();

    void onSingleEntryParsingComplete(DictionaryEntry newEntry);

    //TODO: Replace generic exception with something more specific
    void onParsingError(Exception parsingException);

    void onParsingComplete();

}
