package vessla.languages.dict.kindle.parser;

import vessla.languages.dict.kindle.model.DictionaryEntry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public abstract class AbstractParser {

    private String sourceFile;
    private ArrayList<ParserListener> listeners;

    protected AbstractParser(String sourceFile, ParserListener parserListener){
        this.sourceFile = sourceFile;
        this.listeners = new ArrayList<>();
        if(parserListener != null) {
            this.listeners.add(parserListener);
        }
    }

    public void parse(){
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(getSourceFile()));
            for(ParserListener listener:listeners){
                listener.onParsingStarted();
            }
            while (xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if(xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals(getStartEntryTag())) {
                        DictionaryEntry parsedEntry = parseSingleEntry(xmlEventReader);
                        for(ParserListener listener:listeners){
                            listener.onSingleEntryParsingComplete(parsedEntry);
                        }
                    }
                }
            }
            for(ParserListener listener:listeners){
                listener.onParsingComplete();
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
            if(e instanceof XMLStreamException) {
                for (ParserListener listener : listeners) {
                    listener.onParsingError(e);
                }
            }
        }
    }

    protected abstract String getStartEntryTag();

    protected abstract DictionaryEntry parseSingleEntry(XMLEventReader xmlEventReader) throws XMLStreamException;

    //------------------------------------------------------------------------------------------------------
    // Getters & setters
    //------------------------------------------------------------------------------------------------------

    public String getSourceFile() { return sourceFile; }

    public void setSourceFile(String sourceFile) { this.sourceFile = sourceFile; }

}
