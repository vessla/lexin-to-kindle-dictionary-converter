package vessla.languages.dict.kindle.writer;

import vessla.languages.dict.kindle.model.DictionaryEntry;
import vessla.languages.dict.kindle.model.DictionaryEntryMeaning;
import vessla.languages.dict.kindle.parser.ParserListener;

import java.io.IOException;
import java.io.Writer;

public class KindleWriter implements ParserListener {

    private Writer targetOutput;

    public KindleWriter(Writer targetOutput){
        this.targetOutput = targetOutput;
    }

    @Override
    public void onParsingStarted(){
        try {
            targetOutput.write(getFileHeader());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSingleEntryParsingComplete(DictionaryEntry newEntry) {
        try {
            targetOutput.write(getEntryAsString(newEntry));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //TODO: write to output file here if the returned value is not null
    }

    @Override
    public void onParsingError(Exception parsingException) {
        //TODO: close the stream and remove the file if necessary?
        System.exit(1);
    }

    @Override
    public void onParsingComplete() {
        try {
            targetOutput.write(getFileFooter());
            targetOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getFileHeader(){
        StringBuilder result = new StringBuilder("<!DOCTYPE html>\n<html xmlns:mbp=\"https://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf\"\n");
        result.append("xmlns:idx=\"https://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf\">\n")
                .append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>")
                .append("<body>\n<mbp:frameset>\n");
        return result.toString();
    }

    private static String getFileFooter(){
        return "</mbp:frameset>\n</body>\n</html>";
    }

    private static String getEntryAsString(DictionaryEntry entry){
        StringBuilder result = new StringBuilder("<idx:entry name=\"headword\" scriptable=\"yes\" spell=\"yes\">\n");
        result.append("\t<idx:orth value=\"").append(entry.getBaseIndex()).append("\"><b>").append(entry.getBaseForm()).append("</b>\n");

        if(entry.getIndexes().size()>0) {
            result.append("\t\t<idx:infl>\n");
            for (String index : entry.getIndexes()) {
                result.append("\t\t\t<idx:iform value=\"").append(index).append("\"></idx:iform>\n");
            }
            result.append("\t\t</idx:infl>\n");
        }
        result.append("\t</idx:orth>\n");

        result.append(entry.getType()!=null?"\t<p><em>("+entry.getType()+")</em>":"\t<p>");
        result.append(entry.getInflection()!=null?entry.getInflection()+"</p>\n":"</p>\n");

        if(!entry.isRedirectEntry()) {
            for (int i = 0; i < entry.getMeanings().size(); i++) {
                DictionaryEntryMeaning processedMeaning = entry.getMeanings().get(i);
                result.append("\t<p>").append(i + 1);
                result.append(processedMeaning.getDefinition() != null ? " " + processedMeaning.getDefinition() + "</p>\n" : "</p>\n");

                for (String usage : processedMeaning.getUsages()) {
                    result.append("\t<p>  <em>").append(usage).append("</em></p>\n");
                }
                if (processedMeaning.getExamples().size() > 0) {
                    result.append("\t\t<p style=\"padding:4em\">Exempel</p>\n");
                    for (String example : processedMeaning.getExamples()) {
                        result.append("\t\t<p style=\"padding:6em\">").append(example).append("</p>\n");
                    }
                }
                if (processedMeaning.getIdioms().size() > 0 || processedMeaning.getCompounds().size() > 0) {
                    result.append("\t\t<p style=\"padding:4em\">Uttryck</p>\n");
                    for (String idiom : processedMeaning.getIdioms()) {
                        result.append("\t\t<p style=\"padding:6em\">").append(idiom).append("</p>\n");
                    }
                    for (String compound : processedMeaning.getCompounds()) {
                        result.append("\t\t<p style=\"padding:6em\">").append(compound).append("</p>\n");
                    }
                }
            }
        }
        result.append("</idx:entry>\n");

        return result.toString();
    }
}
