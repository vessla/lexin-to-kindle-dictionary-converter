package vessla.languages.dict.kindle;

import org.kohsuke.args4j.Option;

public class AppCommandLineOptions {

    @Option(name = "-i", usage = "input Lexin .xml file name")
    private String sourceFile = "LEXIN_sample.xml";


    @Option(name = "-o", usage = "output .html file")
    private String targetFile = "kindle/pagea.html";

    //-----------------------------------------------------------------------------------------
    // Getters and setters
    //-----------------------------------------------------------------------------------------

    public String getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    public String getTargetFile() {
        return targetFile;
    }

    public void setTargetFile(String targetFile) {
        this.targetFile = targetFile;
    }

}
