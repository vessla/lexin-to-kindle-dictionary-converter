package vessla.languages.dict.kindle;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import vessla.languages.dict.kindle.parser.AbstractParser;
import vessla.languages.dict.kindle.parser.LexinParser;
import vessla.languages.dict.kindle.writer.KindleWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class App {

    private static Logger logger = Logger.getLogger(App.class);

    public static void main( String[] args )
    {
        AppCommandLineOptions cmdLineOptions = new AppCommandLineOptions();
        CmdLineParser cmdLineParser = new CmdLineParser(cmdLineOptions);

        try {
            cmdLineParser.parseArgument(args);

            BufferedWriter outputWriter = new BufferedWriter(new FileWriter(cmdLineOptions.getTargetFile(), false));
            AbstractParser parser = new LexinParser(cmdLineOptions.getSourceFile(), new KindleWriter(outputWriter));
            parser.parse();
        } catch (CmdLineException e) {
            logger.debug(e.getMessage());
            cmdLineParser.printUsage(System.err);
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }
    }
}
